# HTML-CSS

1. (100%)Ảnh xúc xác thay đổi thành ảnh động mỗi khi ấn sẽ thấy đẹp hơn. tìm hiểu kĩ về cách láy link bootrap này(0%)
2. Css các nút khi hover vào thì chữ và bg đổi thành màu ngược lại của nhau(0%)
3. Tự dow và thay đổi toàn bộ ảnh trong dự án(0%)
4. Hiện bảng table màu đen chữ trắng cho đẹp(0%)
5. Css button ấn vào padding to lên cho nó đẹp(0%)
6. Căn chữ ở table khi ấn thẳng hàng chứ ko nhảy chữ linh tinh(0%)

# JS

6. (100%)User name kiểm tra ko chứa kí tự đặc biệt
7. (100%)validate add them class is validate và khi ấn xong thì xoá class này cho chuyên nghiệp(0%)
8. viết thẻ div có class alert thay cho thẻ p phần hiện câu chúc(0%)
9. Hàm changeVoucherId viết logic hơn để khi check có thể thôgn báo cho cliet "phần thưởng đặc biệt là:"mũ, áo..."(0%)
10. (chưa làm được)viết thẻ div có class alert thông báo lời chúc(0%)
11.

- TASK 23B.40 TDD tham khảo các bước làm được giao diện từ đầu, bootstrap 4.0

| subTask | Yêu cầu                                                                                                                                                                                                                              | Môi trường kiểm tra                           |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------- |
| 0       | Design khung, trên giấy, slide (đã làm sẵn cho bạn, bạn dùng ngay, ko phải làm))                                                                                                                                                     | debug                                         |
| 1       | Tạo một div class container-fluid và một div class container ở phía dưới                                                                                                                                                             | container-fluid, container                    |
| 2       | Tại container 2 tạo 3 div class row                                                                                                                                                                                                  | row                                           |
| 3       | Tại row 2.1 tạo 3 div column có độ rộng đơn vị tỉ lệ là 5:4:3                                                                                                                                                                        | col-5, col-4, col-3                           |
| 4       | Tại cột 2.1.1 tạo 4 div classrow                                                                                                                                                                                                     | row                                           |
| 5       | Tại dòng 2.1.1.1 thêm một div column rộng 12 đơn vị, tại mỗi dòng 2.1.1.2, 2.1.1.3, 2.1.1.4 thêm 2 div column có chiều rộng lần lượt là 4, 8 đơn vị                                                                                  | col-4,col-8 col-12                            |
| 6       | Tại cột 2.1.2 tạo 3 div class row, trong mỗi row chứa 1 cột rộng 12 đơn vị, cột 2.1.2 có thuộc tính căn giữa                                                                                                                         | row, col-12, text-center                      |
| 7       | Tại cột 2.1.3 tạo 2 div class row, trong mỗi row chứa 1 cột rộng 12 đơn vị, cột 2.1.3 có thuộc tính căn giữa                                                                                                                         | row, col-12, text-center                      |
| 8       | Tại row 2.2 và row 2.3 thêm một div column rộng 12 đơn vị                                                                                                                                                                            | col-12                                        |
| 9       | Tại container 1 dùng css để đặt background-image và chiều rộng tối thiểu là 100px, và margin là 5px                                                                                                                                  | background-image, min-height, margin, linkảnh |
| 10      | Thêm 1 thẻ h4 vào cột 2.1.1.1; 1 label vào các cột 2.1.1.2.1, 2.1.1.3.1, 2.1.1.4.1; 1 thẻ input class form-control vào các cột 2.1.1.2.2, 2.1.1.3.2, 2.1.1.4.2; các dòng 2.1.1.1, 2.1.1.2, 2.1.1.3, 2.1.1.4 có thêm class form-group | form-control, form-group                      |
| 11      | Thêm nút ném vào cột 2.1.2.1, ảnh xúc xắc vào cột 2.1.2.2 và thẻ p vào cột 2.1.2.3, các dòng 2.1.2.1, 2.1.2.2, 2.1.2.3 có thêm class form-group                                                                                      | btn, btn-success, img-thumbnail, link ảnh     |
| 12      | Thêm 3 thẻ p vào cột 2.1.3.1 và 1 ảnh quà tặng vào cột 2.1.3.2, các dòng 2.1.3.1, 2.1.3.2 có thêm class form-group                                                                                                                   | Form-group, link ảnh                          |

- Task 23B.50 làm chức năng Ném xúc xắc (new Dice)

| subTask | Yêu cầu                                                                                                                                           | Môi trường kiểm tra |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------- |
| 1       | Ấn nút ném, validate: Nếu user ko điền dữ liệu nào đó, thì báo lỗi ngay, alert Chú ý: chia thành 04 bước xử lý sự kiện click                      | alert               |
| 2       | Gọi server lấy responseText ghi ra console.log(chú ý chia để trị, tách riêng 04 phần)                                                             | console log         |
| 3       | Hãy phân tích JSON (nộp ảnh vào slide TDD) Phân tích cấu trúc json responseText trên trang .Phân tích Json trựctuyếnhttps://jsoneditoronline.org/ | Web                 |
| 4       | Chuyển thành Objects và ghi dữ liệu của object ra console.log                                                                                     | Web                 |
| 5       | Hiển thị Dice mới (3) và lời nhắn (4)                                                                                                             | console log         |
| 6       | Hiển thị voucher (5) nếu có                                                                                                                       | Web                 |
| 7       | Hiển thị prize(6) nếu có                                                                                                                          | Web                 |

- Task 23B.60 làm chức năng lịch sử ném xúc xắc (Dice history)

| subTask                                 | Yêu cầu                                                                                                                                                                                                                       | Môi trường kiểm tra             |
| --------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------- |
| 1                                       | Khi ấn nút Dice History ghi ra console “Nút Dice History được ấn”                                                                                                                                                             | console log                     |
| 2                                       | (tiếp 1) validate: Nếu không có data ở ô input (03 ô) hãy cảnh báo alert                                                                                                                                                      | Web form                        |
| 3                                       | Gọi server lấy responseText ghi ra console.log                                                                                                                                                                                |
| (chú ý chia để trị, tách riêng 04 phần) | console log                                                                                                                                                                                                                   |
| 4                                       | Hãy phân tích JSON (nộp ảnh vào slide TDD)Phân tích cấu trúc json responseText trên trang Phân tích Json trực tuyến https://jsoneditoronline.org/                                                                             | Web                             |
| 5                                       | Chuyển responseText thành object, ghi ra console.log nội dung object                                                                                                                                                          | console.log                     |
| 6                                       | Xóa dữ liệu trong bảng (cả header row) của bảng dùng chung (có id là “ history-placeholder-table”) Sử dụng Javascript để tạo row header mới phù hợp với Dice History Gợi ý: cách tạo row trong thead tương tự như row ở tbody | WebGợi ý:Delete rows from table |
| 7                                       | Hiển thị dữ liệu vào bảng tương đương với số phần tử trong mảng                                                                                                                                                               | Web                             |

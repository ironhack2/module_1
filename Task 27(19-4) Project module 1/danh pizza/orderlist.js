
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_STATUS_CREATE_SUCCESS = 201;
const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // bước 1 truy xuất phần tử table

    // bước 2 kiểm tra với bài này thì k có kiểm tra

    // bước 3 gọi api
    // create a request
    var xhttpGetAllOrder = new XMLHttpRequest;
    xhttpGetAllOrder.open("GET", gBASE_URL, true);
    xhttpGetAllOrder.send();
    xhttpGetAllOrder.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            console.log(xhttpGetAllOrder.responseText)
            // bước 4 xử lý hiển thị
            showTable(xhttpGetAllOrder);
        }
    }
}

// hàm xử lý sự kiện click nút
function onBtnChiTietClick(paramButton) {
    var vOrderID = paramButton.dataset.orderid;
    var vID = paramButton.dataset.id;
    console.log("Order ID: " + vOrderID);
    console.log("ID: " + vID);

    var vLink = "orderdetail.html" + "?" + "orderid=" + vOrderID + "&id=" + vID;
    window.location.href = vLink
}


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

function showTable(paramxhttpGetAllOrder) {
    var vReponseText = JSON.parse(paramxhttpGetAllOrder.responseText);
    var vTable = document.getElementById("table-order")
    var vTbody = vTable.getElementsByTagName("tbody")[0]
    for (var i = 0; i < vReponseText.length; i++) {
        // tạo hàng cuối cho bảng
        var vRow = vTbody.insertRow(-1);
        // tạo các ô trên hàng
        var vCellOrderID = vRow.insertCell(0);
        var vCellKichCo = vRow.insertCell(1);
        var vCellLoaiPizza = vRow.insertCell(2);
        var vCellNuocUong = vRow.insertCell(3);
        var vCellThanhTien = vRow.insertCell(4);
        var vCellHoVaTen = vRow.insertCell(5);
        var vCellSDT = vRow.insertCell(6);
        var vCellTrangThai = vRow.insertCell(7);
        var vCellChiTiet = vRow.insertCell(8);
        // hiển thị lên bảng

        vCellOrderID.innerHTML = vReponseText[i].orderId;
        vCellKichCo.innerHTML = vReponseText[i].kichCo;
        vCellLoaiPizza.innerHTML = vReponseText[i].loaiPizza;
        vCellNuocUong.innerHTML = vReponseText[i].idLoaiNuocUong;
        vCellThanhTien.innerHTML = vReponseText[i].thanhTien;
        vCellHoVaTen.innerHTML = vReponseText[i].hoTen;
        vCellSDT.innerHTML = vReponseText[i].soDienThoai;
        vCellTrangThai.innerHTML = vReponseText[i].trangThai;


        var vButton = document.createElement("button")
        vCellChiTiet.appendChild(vButton);
        vButton.innerHTML = "Chi tiết"
        vButton.className = "btn-detail";


        vButton.setAttribute("data-orderid", vReponseText[i].orderId);
        vButton.setAttribute("data-id", vReponseText[i].id);

        vButton.addEventListener("click", function () {
            onBtnChiTietClick(this);
        })


    }
}


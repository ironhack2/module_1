"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// bạn có thể dùng để lưu trữ combo được chọn,
// mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
var gSelectedMenuStructure = {
        menuName: "...", // S, M, L
        duongKinhCM: 0,
        suongNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0
    }
    // biến toàn cục giá tiền sau khi đc giãm
var gDiscout = {
        maVoucher: "",
        phanTramGiamGia: 0,
        soTienPhaiTra: 0
    }
    // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
var gSelectedPizzaType = "..."
    // lưu thông tin người mua vào 1 biến toàn cục
var gDataPerson = {
    nuocngot: "",
    hovaten: "",
    email: "",
    phone: 0,
    address: "",
    message: "",
    voucher: 0,
}

var gObjectRequest = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
}

var gOrderDetail = {
    id: "",
    orderId: ""
}

// biến kiểm tra api
const gREQUEST_STATUS_OK = 200; // GET & PUT success
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_CREATE_SUCCESS = 201; // status 201 tao thanh cong

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm loading page
function onLoadingPage() {

    "use strict";
    // hàm call api
    var vXhttp = new XMLHttpRequest();
    waterApi(vXhttp);


}

// hàm sự kiện khi nhấn nút chọn combo

// hàm sự kiện khi nhán nút chọn combo S
function onSizeSClick() {
    var vComboS = {
        menuName: "Small",
        duongKinhCM: "20cm",
        suongNuong: 2,
        saladGr: "200gr",
        drink: 2,
        priceVND: 150000
    }
    var vColorBtn = "Small"
    changeColorBtn(vColorBtn);

    gSelectedMenuStructure = vComboS;

    console.log(gSelectedMenuStructure);


}

// hàm sự kiện khi nhán nút chọn combo M
function onSizeMClick() {
    var vComboM = {
        menuName: "Medium",
        duongKinhCM: "25cm",
        suongNuong: 4,
        saladGr: "300gr",
        drink: 2,
        priceVND: 200000
    }

    var vColorBtn = "Medium"
    changeColorBtn(vColorBtn);

    gSelectedMenuStructure = vComboM;

    console.log(gSelectedMenuStructure);
}

// hàm sự kiện khi nhán nút chọn combo L
function onSizeLClick() {
    var vComboL = {
        menuName: "Large",
        duongKinhCM: "30cm",
        suongNuong: 8,
        saladGr: "500gr",
        drink: 4,
        priceVND: 250000
    }

    var vColorBtn = "Large"
    changeColorBtn(vColorBtn);

    gSelectedMenuStructure = vComboL;

    console.log(gSelectedMenuStructure);

}

//hàm sự kiện khi nhấn nút chọn vị pizza

// nút chọn vị hawai
function onBtnHawaiClick() {
    var vPizzaType = "Hawai"
    changeColorBtnType(vPizzaType);
    gSelectedPizzaType = vPizzaType;

}

// nút chọn vị hải sản
function onBtnHaisanClick() {
    var vPizzaType = "Haisan"
    changeColorBtnType(vPizzaType);
    gSelectedPizzaType = vPizzaType;

}

// nút chọn vị thịt hun khói
function onBtnThithunkhoiClick() {
    var vPizzaType = "Thithunkhoi"
    changeColorBtnType(vPizzaType);
    gSelectedPizzaType = vPizzaType;

}

// hàm xử lý sự kiện nhấn nút gửi đơn
function onBtnGuiDon() {
    // truy xuất form data người mua
    var vPerson = getPersonData(gDataPerson);

    // kiểm tra data người mua

    valiDateDataPerson(gDataPerson);
    var vValidate = valiDateDataPerson(gDataPerson);

    //nếu data kiểm tra hợp lệ thì sẽ hiển thị
    if (vValidate) {
        // hàm xử lý voucher
        xuLyVoucher(gDataPerson, gSelectedMenuStructure, gDiscout);
        //hiện thông tin ra
        hienDataPerson(gDataPerson, gSelectedMenuStructure, gSelectedPizzaType, gDiscout)
    }

}

// hàm xử lý sự kiện nhấn nút xác nhận
function onBtnGuiDonClick() {
    var vPerson = document.getElementById("div-data-person")
    var vPizza = document.getElementById("div-data-pizza")
    var vPrice = document.getElementById("div-data-price")
    console.log(vPerson);
    console.log(vPizza);
    console.log(vPrice);
    console.log(gObjectRequest);
    // bước 1 get data (this function no need)
    // bước 2 validate (this function no need)
    // bước 3 call api create order
    var vXhttpCreateOrder = new XMLHttpRequest;
    callApiCreateOrder(gObjectRequest, vXhttpCreateOrder);
    vXhttpCreateOrder.onreadystatechange = function() {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_CREATE_SUCCESS) {
            console.log(vXhttpCreateOrder.responseText)
                // bước 4 show order id in form
            showOrderId(vXhttpCreateOrder);
        }
    }






}

// function handling button chi tiết đơn hàng click
function onBtnChiTietDonHangClick() {
    var vLink = "OrderDetail.html";
    window.location.href = vLink + "?" + "id=" + gOrderDetail.id + "&" + "orderid=" + gOrderDetail.orderId
}

// function load page order detail
function onLoadPageDetail() {
    // access orderid and id by link
    var vLinkDetail = window.location.href;
    var vUrlDetail = new URL(vLinkDetail);

    var vOrderIdDetail = vUrlDetail.searchParams.get("orderid");
    var vIdDetail = vUrlDetail.searchParams.get("id");

    // validate orderid and id
    var vValiDate = valiDateDetail(vOrderIdDetail, vIdDetail);

    if (vValiDate) {
        // call api
        var vXhttp = new XMLHttpRequest();
        callApiGetDataOrder(vXhttp, vOrderIdDetail);
        vXhttp.onreadystatechange =
            function() {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                    console.log(vXhttp.responseText);
                    showDataOrderByOrderId(vXhttp);
                }
            }

    }


}

// function handling button confirm click
function onBtnConfirmClick() {
    "use strict";
    //base url
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    console.log(gOrderDetail.id)
    var vIdOfOrder = gOrderDetail.id;
    var vObjectRequest = {
        trangThai: "confirmed" //3 trang thai open, confirmed, cancel
    }

    var vXmlHttpUpdateOrder = new XMLHttpRequest();
    vXmlHttpUpdateOrder.open("PUT", vBASE_URL + "/" + vIdOfOrder);
    vXmlHttpUpdateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    vXmlHttpUpdateOrder.send(JSON.stringify(vObjectRequest));
    vXmlHttpUpdateOrder.onreadystatechange =
        function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                var vUpdatedOrder = vXmlHttpUpdateOrder.responseText;
                console.log(vUpdatedOrder);
            }
        }
}

// function handling button cancel click
function onBtnCancelClick() {
    "use strict";
    //base url
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    console.log(gOrderDetail.id)
    var vIdOfOrder = gOrderDetail.id;
    var vObjectRequest = {
        trangThai: "cancel" //3 trang thai open, confirmed, cancel
    }

    var vXmlHttpUpdateOrder = new XMLHttpRequest();
    vXmlHttpUpdateOrder.open("PUT", vBASE_URL + "/" + vIdOfOrder);
    vXmlHttpUpdateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    vXmlHttpUpdateOrder.send(JSON.stringify(vObjectRequest));
    vXmlHttpUpdateOrder.onreadystatechange =
        function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                var vUpdatedOrder = vXmlHttpUpdateOrder.responseText;
                console.log(vUpdatedOrder);
            }
        }
}




/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// hàm truy xuất giá trị
// hàm thay đổi thuộc tính màu nút chọn conbo
function changeColorBtn(paramColor) {
    var vBtnSmaill = document.getElementById("btn-size-s");
    var vBtnMedium = document.getElementById("btn-size-m");
    var vBtnLarge = document.getElementById("btn-size-l");

    if (paramColor === "Small") { // nếu là Small thì nút chọn combo S đổi màu vàng còn 2 nút còn lại vẫn màu xanh
        vBtnSmaill.className = "btn-chon"; //gán giá trị cho attribute className đẻ tác động vào class và đổi CSS
        vBtnMedium.className = "btn-khong-chon";
        vBtnLarge.className = "btn-khong-chon";
    } else if (paramColor === "Medium") {
        vBtnSmaill.className = "btn-khong-chon";
        vBtnMedium.className = "btn-chon";
        vBtnLarge.className = "btn-khong-chon";
    } else if (paramColor === "Large") {
        vBtnSmaill.className = "btn-khong-chon";
        vBtnMedium.className = "btn-khong-chon";
        vBtnLarge.className = "btn-chon";
    }

}
// hàm thay đổi thuộc tính màu nút chọn vị
function changeColorBtnType(paramColorBtnType) {
    var vBtnHawai = document.getElementById("btn-hawai");
    var vBtnHaisan = document.getElementById("btn-haisan");
    var vBtnThithunkhoi = document.getElementById("btn-thithunkhoi");

    if (paramColorBtnType === "Hawai") { //nếu chọn PRO thì thay màu nút Pro bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
        vBtnHawai.className = "btn-chon"; //gán giá trị cho attribute className đẻ tác động vào class và đổi CSS
        vBtnHaisan.className = "btn-khong-chon";
        vBtnThithunkhoi.className = "btn-khong-chon";
    } else if (paramColorBtnType === "Haisan") { //nếu chọn Basic thì thay màu nút Pro bằng màu cam
        vBtnHawai.className = "btn-khong-chon";
        vBtnHaisan.className = "btn-chon";
        vBtnThithunkhoi.className = "btn-khong-chon";
    } else if (paramColorBtnType === "Thithunkhoi") { //nếu chọn Premium thì thay màu nút Premium bàng màu cam
        vBtnHawai.className = "btn-khong-chon";
        vBtnHaisan.className = "btn-khong-chon";
        vBtnThithunkhoi.className = "btn-chon";
    }

}
// hàm truy xuất data người đùng đã nhập
function getPersonData(paramDataPerson, ) {
    var vNuocNgot = document.getElementById("input-select")
    var vHoVaTen = document.getElementById("inp-fullname");
    var vEmail = document.getElementById("inp-email");
    var vPhone = document.getElementById("inp-dien-thoai");
    var vAddress = document.getElementById("inp-dia-chi");
    var vMessage = document.getElementById("inp-message");
    var vVoucher = document.getElementById("inp-voucherID");

    paramDataPerson.hovaten = vHoVaTen.value.trim();
    paramDataPerson.email = vEmail.value.trim();
    paramDataPerson.phone = parseInt(vPhone.value.trim());
    paramDataPerson.address = vAddress.value.trim();
    paramDataPerson.message = vMessage.value.trim();
    paramDataPerson.voucher = parseInt(vVoucher.value.trim());
    paramDataPerson.nuocngot = vNuocNgot.value;

    return paramDataPerson;
}
// kiểm tra thông tin người dùng đã nhập
function valiDateDataPerson(paramDataPerson) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (paramDataPerson.nuocngot == "none") {
        alert("hãy chọn loại nước ngọt bạn thích");
        return false;
    }

    if (paramDataPerson.hovaten == "") {
        alert("hãy nhập họ và tên");
        return false;
    }

    if (isNaN(paramDataPerson.hovaten)) {

    } else {
        alert("hãy nhập họ và tên là chữ");
        return false;
    }

    if (paramDataPerson.email.match(mailformat)) {

    } else {
        alert("hãy nhập email đúng")
        return false;
    }

    if (isNaN(paramDataPerson.phone)) {
        alert("hãy nhập số điện thoại");
        return false;
    }

    if (paramDataPerson.address == "") {
        alert("hãy nhập dịa chỉ của bạn");
        return false;
    }

    if (paramDataPerson.voucher == "") {
        paramDataPerson.voucher = 0;
    }

    return paramDataPerson;

}
// hàm hiển thị data
function hienDataPerson(paramDataPerson, paramSelectCombo, paramSelectType, paramdiscount) {

    var vInnerPerson = document.getElementById("div-data-person");
    var vInnerPizza = document.getElementById("div-data-pizza");
    var vInnerPrice = document.getElementById("div-data-price");
    var vContainer = document.getElementById("div-container-order");
    var vSelecNuocNgot = document.getElementById("input-select");

    // hiển thị block
    vContainer.style.display = "block";

    // hiển thị thông tin ra
    vInnerPerson.style.color = "var(--color-xanh-dan)";
    vInnerPerson.style.fontSize = "1.2rem";
    vInnerPerson.style.backgroundColor = "var(--color-vang)";
    vInnerPerson.style.textAlign = "start";
    vInnerPerson.style.fontWeight = "Bolt";
    vInnerPerson.style.padding = "20px"
    vInnerPerson.style.border = "2px solid var(--color-xanh-vua)"
    vInnerPerson.style.borderRadius = "10px"
    vInnerPerson.innerHTML =

        "Họ và Tên: " + paramDataPerson.hovaten + "<br>" +
        "Email: " + paramDataPerson.email + "<br>" +
        "Điện thoại: " + paramDataPerson.phone + "<br>" +
        "Địa chỉ: " + paramDataPerson.address + "<br>" +
        "Lời nhắn: " + paramDataPerson.message + "<br>"

    vInnerPizza.style.color = "var(--color-xanh-dan)";
    vInnerPizza.style.fontSize = "1.2rem";
    vInnerPizza.style.backgroundColor = "var(--color-vang)";
    vInnerPizza.style.textAlign = "start";
    vInnerPizza.style.fontWeight = "Bolt";
    vInnerPizza.style.padding = "20px"
    vInnerPizza.style.border = "2px solid var(--color-xanh-vua)"
    vInnerPizza.style.borderRadius = "10px"
    vInnerPizza.innerHTML =
        "Kích cỡ: " + paramSelectCombo.menuName + "<br>" +
        "Đường kính Pizza: " + paramSelectCombo.duongKinhCM + "<br>" +
        "Sường nướng: " + paramSelectCombo.suongNuong + "<br>" +
        "Salad: " + paramSelectCombo.saladGr + "<br>" +
        "Nước ngọt: " + paramSelectCombo.drink + "<br>"

    vInnerPrice.style.color = "var(--color-xanh-dan)";
    vInnerPrice.style.fontSize = "1.2rem";
    vInnerPrice.style.backgroundColor = "var(--color-vang)";
    vInnerPrice.style.textAlign = "start";
    vInnerPrice.style.fontWeight = "Bolt";
    vInnerPrice.style.padding = "20px"
    vInnerPrice.style.border = "2px solid var(--color-xanh-vua)"
    vInnerPrice.style.borderRadius = "10px"
    vInnerPrice.innerHTML =
        "Loại Pizza: " + paramSelectType + "<br>" +
        "Loại nước uống: " + vSelecNuocNgot.value + "<br>" +
        "Mã giãm giá: " + paramDataPerson.voucher + "<br>" +
        "Giá tiền: " + paramSelectCombo.priceVND + "<br>" +
        "Discount: " + paramdiscount.phanTramGiamGia + "%" + "<br>" +
        "Giá cần thanh toán: " + paramdiscount.soTienPhaiTra + "<br>"

    // gán vào biến toàn cục thông tin order

    gObjectRequest.kichCo = paramSelectCombo.menuName;
    gObjectRequest.duongKinh = paramSelectCombo.duongKinhCM;
    gObjectRequest.suon = paramSelectCombo.suongNuong;
    gObjectRequest.salad = paramSelectCombo.saladGr;
    gObjectRequest.loaiPizza = paramSelectType;
    gObjectRequest.idVourcher = paramDataPerson.voucher;
    gObjectRequest.idLoaiNuocUong = vSelecNuocNgot.value;
    gObjectRequest.soLuongNuoc = paramSelectCombo.drink;
    gObjectRequest.hoTen = paramDataPerson.hovaten;
    gObjectRequest.thanhTien = paramdiscount.soTienPhaiTra;
    gObjectRequest.email = paramDataPerson.email;
    gObjectRequest.soDienThoai = paramDataPerson.phone;
    gObjectRequest.diaChi = paramDataPerson.address;
    gObjectRequest.loiNhan = paramDataPerson.message;

}

// hàm xử lý voucher

function xuLyVoucher(paramDataPerson, paramSelectedMenuStructure, paramDiscout) {
    //khai báo gửi thông tin lên sever
    var vXmlHttp = new XMLHttpRequest();
    var vLinkRequest = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramDataPerson.voucher;

    vXmlHttp.open("GET", vLinkRequest, false);
    vXmlHttp.send();
    if (vXmlHttp.status == gREQUEST_STATUS_OK) {

        // nhận dữ liệu sever gửi về
        var vJsonStringResponse = vXmlHttp.responseText;
        console.log("responseText = " + vJsonStringResponse);

        // chuyển sang object

        var vObjectResponse = JSON.parse(vJsonStringResponse);
        console.log(vObjectResponse);
        // gán giá trị voucher mới nhận được từ sever vào biến gDiscout
        paramDiscout.maVoucher = vObjectResponse.maVoucher;
        paramDiscout.phanTramGiamGia = parseInt(vObjectResponse.phanTramGiamGia);
        paramDiscout.soTienPhaiTra = paramSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * (vObjectResponse.phanTramGiamGia * 0.01));

        return paramDiscout;

    } else {
        paramDataPerson.voucher = "voucher không tồn tại";
        paramDiscout.phanTramGiamGia = 0;
        paramDiscout.soTienPhaiTra = paramSelectedMenuStructure.priceVND;

        return paramDiscout;

    }




}


function waterApi(paramXhttp) {
    var vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
    paramXhttp.open("GET", vBASE_URL, true);
    paramXhttp.send();
    paramXhttp.onreadystatechange = function() {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {

            console.log((paramXhttp.responseText));

            var bSelect = JSON.parse(paramXhttp.responseText);


            // truy xuất ô select

            var vInpSelect = document.getElementById("input-select");

            for (var bJ = 0; bJ < bSelect.length; bJ++) {

                var vNameOptions = document.createElement("option");
                vNameOptions.value = bSelect[bJ].maNuocUong;
                vNameOptions.text = bSelect[bJ].tenNuocUong;

                vInpSelect.appendChild(vNameOptions);
            }
        }
    }



}
// function call api create order
function callApiCreateOrder(paramObjectRequest, paramXhttpCreateOrder) {
    //base url
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";

    paramXhttpCreateOrder.open("POST", vBASE_URL, true);
    paramXhttpCreateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    paramXhttpCreateOrder.send(JSON.stringify(paramObjectRequest));
}

// function handling show orderid on form

function showOrderId(paramXhttpCreateOrder) {
    var vReponseCreateOrder = JSON.parse(paramXhttpCreateOrder.responseText)
        // access element
    var vFormOrderID = document.getElementById("div-container-orderid");
    var vInpOrderID = document.getElementById("inp-orderid");
    var vFormDataOrder = document.getElementById("div-container-order");

    vFormOrderID.style.display = "block";
    vFormDataOrder.style.display = "none";
    vInpOrderID.value = vReponseCreateOrder.orderId;

    gOrderDetail.id = vReponseCreateOrder.id;
    gOrderDetail.orderId = vReponseCreateOrder.orderId;

    return gOrderDetail;
}

// function validate orderid and id on page order detail
function valiDateDetail(paramOrderIdDetail, paramIdDetail) {
    if (paramOrderIdDetail === "") {
        alert("không có đơn hàng!");
        window.location.href = "pizza.html";
        return false;
    }
    if (paramIdDetail === "") {
        alert("không có đơn hàng!");
        window.location.href = "pizza.html";
        return false;
    }
    return true;
}

// function handling call api get data order
function callApiGetDataOrder(paramXhttp, paramOrderIdDetail) {
    "use strict";
    //base url
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";

    paramXhttp.open("GET", vBASE_URL + "/" + paramOrderIdDetail, true);
    paramXhttp.send();

}

// function handling show data order detail on form
function showDataOrderByOrderId(paramXhttp) {
    //parse json string to object
    var vOjbOrderInfo = JSON.parse(paramXhttp.responseText);
    // access element form
    var vOrderID = document.getElementById("inp-orderid");
    var vCombo = document.getElementById("inp-combo");
    var vDuongKinh = document.getElementById("inp-duong-kinh");
    var vSuongNuong = document.getElementById("inp-suong-nuong");
    var vDoUong = document.getElementById("inp-do-uong");
    var vSoLuongNuocUong = document.getElementById("inp-so-luong-nuoc");
    var vVoucherID = document.getElementById("inp-voucherid");
    var vLoaiPizza = document.getElementById("inp-loai-pizza");
    var vSalad = document.getElementById("inp-salad");
    var vThanhTien = document.getElementById("inp-thanh-tien");
    var vGiamGia = document.getElementById("inp-giam-gia");
    var vHoTen = document.getElementById("inp-hovaten");
    var vEmail = document.getElementById("inp-email");
    var vSoDienThoai = document.getElementById("inp-sdt");
    var vDiaChi = document.getElementById("inp-dia-chi");
    var vLoiNhan = document.getElementById("inp-loi-nhan");
    var vTrangThaiDonHang = document.getElementById("inp-status");
    var vNgayTao = document.getElementById("inp-create");
    var vNgayCapNhat = document.getElementById("inp-update");


    vOrderID.value = vOjbOrderInfo.orderId;
    vCombo.value = vOjbOrderInfo.kichCo;
    vDuongKinh.value = vOjbOrderInfo.duongKinh;
    vSuongNuong.value = vOjbOrderInfo.suon;
    vDoUong.value = vOjbOrderInfo.idLoaiNuocUong;
    vSoLuongNuocUong.value = vOjbOrderInfo.soLuongNuoc;
    vVoucherID.value = vOjbOrderInfo.idVourcher;
    vLoaiPizza.value = vOjbOrderInfo.loaiPizza;
    vSalad.value = vOjbOrderInfo.salad;
    vThanhTien.value = vOjbOrderInfo.thanhTien;
    vGiamGia.value = vOjbOrderInfo.giamGia;
    vHoTen.value = vOjbOrderInfo.hoTen;
    vEmail.value = vOjbOrderInfo.email;
    vSoDienThoai.value = vOjbOrderInfo.soDienThoai;
    vDiaChi.value = vOjbOrderInfo.diaChi;
    vLoiNhan.value = vOjbOrderInfo.loiNhan;
    vTrangThaiDonHang.value = vOjbOrderInfo.trangThai;
    vNgayTao.value = vOjbOrderInfo.ngayTao;
    vNgayCapNhat.value = vOjbOrderInfo.ngayCapNhat;

    gOrderDetail.orderId = vOjbOrderInfo.orderId;
    gOrderDetail.id = vOjbOrderInfo.id;
}